﻿using CaseApplication.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CaseApplication.ViewModel
{
    public class CarDetailViewModel
    {
        public List<string> temp = new List<string>();

        public string Answer { get; set; }   
     
        



        public string Co2Calculation(List<string> co2value)
        {
            List<int> temp = new();
            foreach (string item in co2value) 
            {
                
                int numericCo2value = Int32.Parse(item);
                int NumberOfTrees = (numericCo2value * 25000) / 22000;
                temp.Add(NumberOfTrees);
                
            }
            
             Answer = temp.Sum().ToString();

            return this.Answer;
        }

    }
}
