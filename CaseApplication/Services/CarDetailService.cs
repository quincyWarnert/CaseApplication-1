﻿using CaseApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace CaseApplication.Services
{
    public class CarDetailService : ICarDetailService
    {

        private HttpClient _httpClient;

        public CarDetailService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<string> Get(string plate)
        {
        
           
            if (plate == null) {
                return null;
            }
           
            string ApiUrl = $"?kenteken={plate.ToUpper().Trim()}";
            var response = _httpClient.GetStreamAsync(ApiUrl);
            var repositories = await JsonSerializer.DeserializeAsync<List<CarDetail>>(await response);
             foreach(var repo in repositories)
            {
                return repo.co2_uitstoot_gecombineerd;
            }
            return null;            
                  
         }            
          

            
        
    }
}
