﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CaseApplication.Services
{
    public interface ICarDetailService
    {
        Task<string> Get(string plate);
    }
}
