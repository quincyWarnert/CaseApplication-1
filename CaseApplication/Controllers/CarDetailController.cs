﻿using CaseApplication.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CaseApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarDetailController : ControllerBase
    {
        private readonly ICarDetailService _carService;

        public CarDetailController(ICarDetailService carService)
        {
           
           _carService = carService;
        }
        // GET: api/<CarDetailController>
        [HttpPost]
        public async Task<string> Get(string plate)
        {
            
            
            return await _carService.Get(plate);
        }

        // GET api/<CarDetailController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<CarDetailController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<CarDetailController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<CarDetailController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
