﻿using CaseApplication.Models;
using CaseApplication.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CaseApplication.Controllers;
using CaseApplication.Services;
using System.Net.Http;

namespace CaseApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private  ICarDetailService _carDetailService;
        CarDetailViewModel car = new CarDetailViewModel();
        

        public HomeController(ILogger<HomeController> logger, ICarDetailService carDetailService)
        {
            _logger = logger;
            _carDetailService =carDetailService;
        }

        public IActionResult Index()
        {
            return View(car);
        }
        [HttpPost]
        public async Task<IActionResult>  Index(string[] userInput)
        {
            List<string> plateList = new();
            foreach (string item in userInput)
            {
                string checkNull = _carDetailService.Get(item).Result;
                if (checkNull != null) 
                {
                    plateList.Add(checkNull);
                }
            
            }

             car.Co2Calculation(plateList);
            

            return  await Task.Run(()=>View(car));

        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
